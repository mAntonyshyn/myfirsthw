<?php
class FormBuilder
{
    private function main($mass)
    {
        $a = "";

        foreach ($mass as $key=>$value){
            $a .= " $key=\"$value\" ";

        }
        return $a;
    }
    public function open($mass)
    {
        return "<form ".$this->main($mass).">";
    }

    public function input($mass)
    {
        return "<input ".$this->main($mass).">";
    }

    public function submit($mass)
    {
        return "<input type=\"submit\" ".$this->main($mass).">";
    }

    public function close()
    {
        return "</form>";
    }
}