<?php
class WorkerAlpha
{
    public $name;
    public $age;
    public $salary;
}
echo "<h3>Завдання 1.1</h3>";
$obj1 = new WorkerAlpha();
$obj1->name = "Іван";
$obj1->age = 25;
$obj1->salary = 1000;

$obj2 = new WorkerAlpha();
$obj2->name = "Вася";
$obj2->age = 26;
$obj2->salary = 2000;

echo $obj1->salary + $obj2->salary."<br>";
echo $obj1->age + $obj2->age."<br>";

class WorkerBeta
{
    private $name;
    private $age;
    private $salary;

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }
    public function getAge(){
        return $this->age;
    }
    public function setAge($age){
        $this->age = $age;

    }
    public function getSalary(){
        return $this->salary;
    }
    public function setSalary($salary){
        $this->salary = $salary;

    }
}

echo "<h3>Завдання 1.2</h3>";
$worker1 = new WorkerBeta();
$worker1->setName("Іван");
$worker1->setAge(25);
$worker1->setSalary(1000);


$worker2 = new WorkerBeta();
$worker2->setName("Вася");
$worker2->setAge(26);
$worker2->setSalary(2000);

echo $worker1->getSalary() + $worker2->getSalary()."<br>";
echo $worker1->getAge() + $worker2->getAge()."<br>";


class WorkerGamma
{
    private $name;
    private $age;
    private $salary;

    private function checkAge($age){
        if( $age<= 100 && $age>=1){
            return true;
        }else{
            return false;
        }
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }
    public function getAge(){
        return $this->age;
    }
    public function setAge($age){
        if($this->checkAge($age)) {
            $this->age = $age;
        }else{
            echo "Error";
        }
    }
    public function getSalary(){
        return $this->salary;
    }
    public function setSalary($salary){
        $this->salary = $salary;

    }

}
echo "<h3>Завдання 1.3</h3>";
$worke = new WorkerGamma();
//$worke->setName("Іван");
$worke->setAge(22);
//$worke->setSalary(1000);
echo $worke->getAge()."<br>";

class Calculate
{
    private $a;
    private $b;

    function __construct($a, $b) {
        $this->a = $a;
        $this->b = $b;
    }
    public function add(){
        return ($this->a) + ($this->b);
    }
    public function subtraction(){
        return ($this->a) - ($this->b);
    }
    public function multiplication(){
        return ($this->a) * ($this->b);
    }
    public function division(){
//        if($this->b !== 0) {
//            return ($this->a) / ($this->b);
//        }else{
//            return "Ділення на нуль";
//
//        }
        try {
            return ($this->a) / ($this->b);
        } catch (Exception $e) {
            return $e->getMessage(). "\n";
        }
    }
}
echo "<h3>Завдання 2</h3>";
$myCalc = new Calculate(10,0);
echo "Сумма a i b = ".$myCalc->add()."<br>"."Віднімання числі b від a = ".$myCalc->subtraction()."<br>"."Множення a i b = ".$myCalc->multiplication()."<br>"."Ділення a на b = ".$myCalc->division();
