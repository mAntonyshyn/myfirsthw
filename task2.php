<?php
$name = $_POST["name"];
$format = ".".pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);

function changeName(){
    global $name, $format;
    move_uploaded_file($_FILES["file"]["tmp_name"],__DIR__."\\".$name.$format);
}

switch ($format){
    case ".json":
    case ".xls":
    case ".png":
    case ".xml":
        changeName();
        echo "Готово";
    break;
    default:
       echo "Invalid format. Only .json, .xml, .png and .xls are supported";
}